package screech.drive.swerve;

//import com.revrobotics.CANSparkMax
//import com.revrobotics.CANSparkMaxLowLevel
//import com.revrobotics.CANSparkMax.IdleMode
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import edu.wpi.first.wpilibj.PIDController;
import frc.robot.RobotMap;
import screech.control.Utilities;
import screech.enums.WheelLocation;
import screech.models.PidConstants;
import screech.sensors.SwerveModuleEncoder;

/**
 * Describes a swerve module in 2333's swerve drive chassis
 */
public class Wheel {
    public SwerveModuleEncoder HeadingEncoder;
    public WPI_TalonFX DriveMotor;
    public WPI_TalonFX RotatorMotor;
    public PIDController RotatorPidController;

    public Wheel(WheelLocation location, int driveMotorChannel, int turnMotorChannel, int encoderChannel, double rotatorOffset, PidConstants pidConstants) {
        System.out.println("[SWERVE] Created new " + location.toString() + " swerve wheel");
        System.out.println("========== Drive Channel: " + driveMotorChannel);
        System.out.println("========== Rotator Channel: " + turnMotorChannel);
        System.out.println("========== Encoder Channel: " + encoderChannel);
        System.out.println("========== Encoder Offset: " + rotatorOffset);
        
        //DriveMotor = new CANSparkMax(driveMotorChannel, CANSparkMaxLowLevel.MotorType.kBrushless);
        //DriveMotor.clearFaults();
        //DriveMotor.setIdleMode(IdleMode.kBrake);

        DriveMotor = new WPI_TalonFX(driveMotorChannel);
        DriveMotor.clearStickyFaults();
        DriveMotor.setNeutralMode(NeutralMode.Brake);

        //RotatorMotor = new CANSparkMax(turnMotorChannel, CANSparkMaxLowLevel.MotorType.kBrushless);
        //RotatorMotor.clearFaults();
        //RotatorMotor.setInverted(true);
        //RotatorMotor.setIdleMode(IdleMode.kBrake);

        RotatorMotor = new WPI_TalonFX(turnMotorChannel);
        RotatorMotor.clearStickyFaults();
        RotatorMotor.setInverted(true);
        RotatorMotor.setNeutralMode(NeutralMode.Brake);





        HeadingEncoder = new SwerveModuleEncoder(encoderChannel, rotatorOffset);

        RotatorPidController = new PIDController(pidConstants.kP, pidConstants.kI, pidConstants.kD, 0, HeadingEncoder, RotatorMotor, 0.02);
        RotatorPidController.setInputRange(-180, 180);
        RotatorPidController.setOutputRange(-1, 1);
        RotatorPidController.setPercentTolerance(2);
        RotatorPidController.setContinuous(true);
        RotatorPidController.setEnabled(true);
    }

    public void SetDriveInverted(boolean inverted) {
        DriveMotor.setInverted(inverted);
    }

    // Control
    
    public void StopAllMotors() {
        DriveMotor.stopMotor();
        RotatorMotor.stopMotor();
    }

    public void SetAngle(double angleSetpoint) {
        RotatorPidController.setSetpoint(angleSetpoint);
    }

    public void SetMagnitude(double magnitude) {
        double gainedSpeed = magnitude * RobotMap.SpeedGain;
        double finalSpeed = Utilities.Deadband(gainedSpeed, RobotMap.SpeedDeadband);
        DriveMotor.set(finalSpeed);   
    }

    public void SetWheel(double magnitude, double angle) {
        SetMagnitude(magnitude);
        if (magnitude > RobotMap.SpeedDeadband) {
            SetAngle(angle);
        }
    }

    // Telemetry

    /**
     * Get current robot-centric angle heading of this wheel
     * @return Robot-centric wheel heading 0-360°
     */
    public double GetCurrentHeading() { 
        return HeadingEncoder.GetRelativeAngle(); 
    }

    /**
     * Get temperature of the motors
     * @return Array[2] of motor temperatures [Drive, Rotator]
     */
    public double[] GetMotorTemps() {
        double[] temps = new double[] { DriveMotor.getTemperature(), RotatorMotor.getTemperature() };
        return temps;
    }

    /**
     * Get current draw of the motors
     * @return Array[2] of motor currents [Drive, Rotator]
     */
    public double[] GetMotorOutputCurrents() {
        double[] currents = new double[] { DriveMotor.getOutputCurrent(), RotatorMotor.getOutputCurrent() };
        return currents;
    }

    /**
    public short GetDriverStickyFaults() {
        return DriveMotor.getStickyFaults(StickyFaults.UnderVoltage);
    }

    public short GetRotatorStickyFaults() {
        return RotatorMotor.getStickyFaults();
    }
    */
}