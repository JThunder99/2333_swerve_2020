package screech.drive.swerve;

import screech.enums.WheelLocation;
import screech.models.PidConstants;

public class SwerveMap {
    private WheelMap _frontRight, _frontLeft, _rearRight, _rearLeft;
    public double WheelbaseL, WheelbaseW;
    public PidConstants RotatorPid;

    public SwerveMap(int frdc, int fldc, int rrdc, int rldc, int frrc, int flrc, int rrrc, int rlrc, int frec, int flec, int rrec, int rlec, int freo, int fleo, int rreo, int rleo, double wbl, double wbw, PidConstants rotatorPid) {
        _frontRight = new WheelMap(frdc, frrc, frec, freo);
        _frontLeft = new WheelMap(fldc, flrc, flec, fleo);
        _rearRight = new WheelMap(rrdc, rrrc, rrec, rreo);
        _rearLeft = new WheelMap(rldc, rlrc, rlec, rleo);
        WheelbaseL = wbl;
        WheelbaseW = wbw;
        RotatorPid = rotatorPid;
    }

    public int GetDriveMotorChannel(WheelLocation location) throws Exception {
        switch (location) {
            case kFR:
                return _frontRight.DriveMotorChannel;
            case kFL:
                return _frontLeft.DriveMotorChannel;
            case kRR:
                return _rearRight.DriveMotorChannel;
            case kRL:
                return _rearLeft.DriveMotorChannel;
            default:
                throw new Exception("Could not get drive motor channel. Invalid Location");
        }
    }

    public int GetRotatorMotorChannel(WheelLocation location) throws Exception {
        switch (location) {
            case kFR:
                return _frontRight.RotatorMotorChannel;
            case kFL:
                return _frontLeft.RotatorMotorChannel;
            case kRR:
                return _rearRight.RotatorMotorChannel;
            case kRL:
                return _rearLeft.RotatorMotorChannel;
            default:
                throw new Exception("Could not get rotator motor channel. Invalid Location");
        }
    }

    public int GetEncoderChannel(WheelLocation location) throws Exception {
        switch (location) {
            case kFR:
                return _frontRight.EncoderChannel;
                
            case kFL:
                return _frontLeft.EncoderChannel;
            case kRR:
                return _rearRight.EncoderChannel;
            case kRL:
                return _rearLeft.EncoderChannel;
            default:
                throw new Exception("Could not get encoder channel. Invalid Location");
        }
    }

    public double GetEncoderOffset(WheelLocation location) throws Exception {
        switch (location) {
            case kFR:
                return _frontRight.EncoderOffset;
            case kFL:
                return _frontLeft.EncoderOffset;
            case kRR:
                return _rearRight.EncoderOffset;
            case kRL:
                return _rearLeft.EncoderOffset;
            default:
                throw new Exception("Could not get encoder Offset. Invalid Location");
        }
    }
}