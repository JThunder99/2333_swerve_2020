package screech.enums;

public enum WheelLocation {
    kFL,
    kFR,
    kRL,
    kRR
}