package frc.robot;

import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import frc.robot.subsystems.TurretSubsystem;
import screech.control.Utilities;
import screech.control.XboxInput;
import screech.drive.swerve.*;
import screech.sensors.AS5600EncoderPwm;
import screech.sensors.SwerveModuleEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import com.revrobotics.CANSparkMax.IdleMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import screech.drive.swerve.Wheel;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.Compressor;
//import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
//import edu.wpi.first.networktables.NetworkTable;
//import edu.wpi.first.networktables.NetworkTableEntry;
//import edu.wpi.first.networktables.NetworkTableInstance;

public class Robot extends TimedRobot {
  private SwerveChassis drive;
  private TurretSubsystem Turret;
  Compressor compressor;
  CANSparkMax leftSparkMax;
  CANSparkMax rightSparkMax;
  WPI_TalonSRX outerintakeTalon;
  WPI_TalonSRX innerintakeTalon;
  WPI_TalonSRX turretTalon;
  DoubleSolenoid hood_tilt;
  DoubleSolenoid outerintake_tilt;
  AS5600EncoderPwm turretEncoder;
  XboxInput driveController;
  XboxInput operatorController;
  AHRS newGyro;
  static boolean DebugEnabled = true;
  boolean toggleOuterIntakeOn = false;
  boolean toggleHoodTiltOn = false;
  boolean toggleSpeedReductionOn = false;
  boolean toggleOuterIntakeTiltOn = false;
  boolean toggleOuterIntakePressed = false;
  boolean toggleHoodTiltPressed = false;
  boolean toggleInnerIntakePressed = false;
  boolean toggleOuterIntakeTiltPressed = false;
  boolean toggleSpeedReductionPressed = false;

  NetworkTable LimelightTable;
  NetworkTableEntry LimelightTx;
  NetworkTableEntry LimelightTy;
  NetworkTableEntry LimelightTa;
  NetworkTableEntry LimelightTv;

  double DLimelightTx;
  double DLimelightTy;
  double DLimelightTa;
  double DLimelightTv;

  WPI_TalonFX AutoDriveFR;
  WPI_TalonFX AutoDriveFL;
  WPI_TalonFX AutoDriveRR;
  WPI_TalonFX AutoDriveRL;

  AddressableLED m_led;
  AddressableLEDBuffer m_ledBuffer;

  CameraServer server;


  @Override
  public void robotInit() {

    AutoDriveFR = new WPI_TalonFX(10);
    AutoDriveFL = new WPI_TalonFX(12);
    AutoDriveRR = new WPI_TalonFX(15);
    AutoDriveRL = new WPI_TalonFX(16);

    //Compressor Stuff
    compressor = new Compressor();
    compressor.setClosedLoopControl(true);
    compressor.start();

    //Swerve Drive
    try {
      // Create a new drive chassis
      drive = new SwerveChassis(RobotMap.SwerveMap);
      
      // Initialize NavX IMU
      newGyro = new AHRS(SerialPort.Port.kUSB);
    } catch (Exception e) {
      DriverStation.reportError(e.getMessage(), e.getStackTrace());
    }

    // TurretSubsystem
    Turret = new TurretSubsystem();

    // 2 Spark Max Shooter Motors
    leftSparkMax = new CANSparkMax(18, CANSparkMaxLowLevel.MotorType.kBrushless);
    leftSparkMax.clearFaults();
    leftSparkMax.setIdleMode(IdleMode.kCoast);

    rightSparkMax = new CANSparkMax(19, CANSparkMaxLowLevel.MotorType.kBrushless);
    rightSparkMax.clearFaults();
    rightSparkMax.setIdleMode(IdleMode.kCoast);

    // Outer intake 775 motor controlled by a Talon SRX
    outerintakeTalon = new WPI_TalonSRX(20);
    outerintakeTalon.clearStickyFaults();
    outerintakeTalon.setNeutralMode(NeutralMode.Brake);

    // Inner intake 775 motor controlled by a Talon SRX
    innerintakeTalon = new WPI_TalonSRX(21);
    innerintakeTalon.clearStickyFaults();
    innerintakeTalon.setNeutralMode(NeutralMode.Brake);

    // Pneumatic Hood Tilt 
    hood_tilt = new DoubleSolenoid(0, 1);
    hood_tilt.clearAllPCMStickyFaults();

    // Pneumatic Outer Intake Tilt
    outerintake_tilt = new DoubleSolenoid(2, 3);
    outerintake_tilt.clearAllPCMStickyFaults();

    // Reset NavX Gyro
    newGyro.reset();

    // Initialize xbox controllers
    driveController = new XboxInput(0);
    operatorController = new XboxInput(1);
    
    // Limelight Stuff
    LimelightTable = NetworkTableInstance.getDefault().getTable("limelight");
    LimelightTx = LimelightTable.getEntry("tx"); 
    LimelightTy = LimelightTable.getEntry("ty"); 
    LimelightTa = LimelightTable.getEntry("ta");
    LimelightTv = LimelightTable.getEntry("tv");
    DLimelightTx = LimelightTx.getDouble(0.0);
    DLimelightTy = LimelightTy.getDouble(0.0);
    DLimelightTa = LimelightTa.getDouble(0.0);
    DLimelightTv = LimelightTv.getDouble(0.0);

    // USB Camera
    server = CameraServer.getInstance();
    server.startAutomaticCapture(0);

    // LED Control on PWM port 0
    m_led = new AddressableLED(0);
    m_ledBuffer = new AddressableLEDBuffer(12);
    m_led.setLength(m_ledBuffer.getLength());
    m_led.setData(m_ledBuffer);
    m_led.start();
  }

  @Override
  public void robotPeriodic() {
  }

  @Override
  public void disabledPeriodic() {

  }

  @Override
  public void autonomousInit() {
    
  }

  @Override
  public void autonomousPeriodic() {
    // System.out.println("Ta" + DLimelightTx);
    // System.out.println("Tx" + DLimelightTx);
    // System.out.println("Ty" + DLimelightTy);
    // System.out.println("Tv" + DLimelightTv);
    // System.out.println("Locked On: " + Turret.LockedOn);
    // System.out.println("Rot PID: " + Turret.RotationPID);

    //Start up Flywheel motors to 40% power
    double AutoSpeed = -.4;
    leftSparkMax.set(AutoSpeed * -1);
    rightSparkMax.set(AutoSpeed);
    
    //Auto align to target
    // boolean ToggleLock = true;
    // if (ToggleLock = true) {
    // Turret.ToggleLockOn();
    // }
    // var turretControl = 1;
    // if (Turret.LockedOn || Turret.RotationPID.isEnabled()) {
    //   // If locked on, use limelight
    //   if (Turret.LockedOn) {
    //     // 
    //     if (!(LimelightTable.getEntry("tv").getNumber(Integer.MIN_VALUE).intValue() == 1)) {
    //       Turret.LockedOn = false;
    //       Turret.SetPIDEnabled(false);
    //     } else {
    //       var manualAdjustment = turretControl * 10;
    //       Turret.SetRotationWithLimelightOffset(LimelightTx.getDouble(0), manualAdjustment);
    //     }
    //   }
    // } 

    // After 4 seconds set inner intake to 35% power
    Timer.delay(4.0);
    innerintakeTalon.set(.35);
    // After 4 seconds set drive wheels to 50% to drive forward
    Timer.delay(4.0);
    AutoDriveFR.set(.5);
    AutoDriveFL.set(.5);
    AutoDriveRR.set(.5);
    AutoDriveRL.set(.5);
    // After one second of drive motors moving shut off drive motors
    Timer.delay(1.0);
    AutoDriveFR.set(0);
    AutoDriveFL.set(0);
    AutoDriveRR.set(0);
    AutoDriveRL.set(0);
  }

  @Override
  public void teleopInit() { 
    // Reset NavX Gyro
    newGyro.reset();
  }

  @Override
  public void teleopPeriodic() {
    // Pulling NavX gyro angle
    double gyroAngle = Utilities.ClampContinuousAngle(newGyro.getAngle());

    // Subtract trigger 1 from trigger 2 to get a full -1 - 1 range and add a 0.1 deadband
    double triggerAxis = Utilities.Deadband(driveController.getRawAxis(2) - driveController.getRawAxis(3), 0.1);
    
    // Field-centric swerve drive
    drive.SwerveDrive(gyroAngle, (triggerAxis * 0.75), -driveController.getX(Hand.kLeft), -driveController.getY(Hand.kLeft));
    
    // Reset gyro on right bumper press
    if (driveController.getRawButton(6)) {
      SendDebug("Resetting Gyro");
      newGyro.reset();
    }

    // Flywheel minimum speed of 40% and left trigger up to 75% power
    double Speed = (operatorController.getRawAxis(2) * .75);
    if (Speed > .41){
      leftSparkMax.set(Speed);
      rightSparkMax.set(Speed* -1);
    }else{
      leftSparkMax.set(.4);
      rightSparkMax.set(.4 * -1);
    }
    
    // Outer Intake control
    updateOuterIntakeToggle();
    if(toggleOuterIntakeOn){
      outerintakeTalon.set(-.75);
      }else{
        outerintakeTalon.set(0);
    }

    // Right trigger control for inner intake at 35% speed
    double intakeTriggerSpeed = operatorController.getRawAxis(3) * .35;
    innerintakeTalon.set(intakeTriggerSpeed);

    // Speed Reduction control for Swerve Drive
    updateSpeedReductionToggle();
    if(toggleSpeedReductionOn){
      RobotMap.SpeedGain = 0.75;
      }else{
        RobotMap.SpeedGain = 0.5;
      }

    // Pneumatic hood tilt control
    updateHoodTiltToggle();
    if(toggleHoodTiltOn){
      hood_tilt.set(DoubleSolenoid.Value.kReverse);
      }else{
        hood_tilt.set(DoubleSolenoid.Value.kForward);
    }

    // Pneumatic outer intake tilt control
    updateOuterIntakeTiltToggle();
    if(toggleOuterIntakeTiltOn){
      outerintake_tilt.set(DoubleSolenoid.Value.kForward);
      }else{
        outerintake_tilt.set(DoubleSolenoid.Value.kReverse);
    }

    // Left bumper control for turret lockon using limelight
    if (operatorController.getRawButton(5)) {
      Turret.ToggleLockOn();
    }

    // Limelight Stuff
    LimelightTable = NetworkTableInstance.getDefault().getTable("limelight");
    LimelightTx = LimelightTable.getEntry("tx"); 
    LimelightTy = LimelightTable.getEntry("ty"); 
    LimelightTa = LimelightTable.getEntry("ta");
    LimelightTv = LimelightTable.getEntry("tv");
    DLimelightTx = LimelightTx.getDouble(0.0);
    DLimelightTy = LimelightTy.getDouble(0.0);
    DLimelightTa = LimelightTa.getDouble(0.0);
    DLimelightTv = LimelightTv.getDouble(0.0);
    // System.out.println("Ta" + DLimelightTx);
    // System.out.println("Tx" + DLimelightTx);
    // System.out.println("Ty" + DLimelightTy);
    // System.out.println("Tv" + DLimelightTv);

    // Turret controls
    var turretControl = operatorController.getRawAxis(4);
    if (Turret.LockedOn || Turret.RotationPID.isEnabled()) {
      // If locked on, use limelight
      if (Turret.LockedOn) {
        if (!(LimelightTable.getEntry("tv").getNumber(Integer.MIN_VALUE).intValue() == 1)) {
          Turret.LockedOn = false;
          Turret.SetPIDEnabled(false);
        } else {
          var manualAdjustment = turretControl * 10;
          Turret.SetRotationWithLimelightOffset(LimelightTx.getDouble(0), manualAdjustment);
        }
      }
    } else {
      Turret.RotateManually(turretControl * 0.5);
    }
    
    // LED indicator for Limelight locked on
    for (var i = 0; i < m_ledBuffer.getLength(); i++){
      if (Turret.LockedOn){
          m_ledBuffer.setRGB(i, 0, 255, 0);
          m_led.setData(m_ledBuffer);      
      }else{
            m_ledBuffer.setRGB(i, 255, 0, 0);
            m_led.setData(m_ledBuffer);
        }
    }
  }

  // Helper method for System.out.println
  public static void SendDebug(String string) {
    if (!DebugEnabled)
      return;

    System.out.println(string);
  }

  @Override
  public void disabledInit() {
    newGyro.reset();
    drive.StopAllMotors();
  }

  // UpdateOuterIntakeToggle
  public void updateOuterIntakeToggle(){
    if(operatorController.getRawButton(1)){
      if(!toggleOuterIntakePressed){
        toggleOuterIntakeOn = !toggleOuterIntakeOn;
        toggleOuterIntakePressed = true;
        }
        }else{                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        toggleOuterIntakePressed = false;
        }
    }

  // UpdateSpeedReductionToggle
  public void updateSpeedReductionToggle(){
    if(driveController.getRawButton(1)){
      if(!toggleInnerIntakePressed){
        toggleSpeedReductionOn = !toggleSpeedReductionOn;
        toggleSpeedReductionPressed = true;
        }
        }else{
          toggleSpeedReductionPressed = false;
        }
     }

  // UpdateHoodTiltToggle
  public void updateHoodTiltToggle(){
    if(operatorController.getRawButton(3)){
      if(!toggleHoodTiltPressed){
        toggleHoodTiltOn = !toggleHoodTiltOn;
        toggleHoodTiltPressed = true;
        }
        }else{
        toggleHoodTiltPressed = false;
        }
     }

  // UpdateOuterIntakeTiltToggle    
  public void updateOuterIntakeTiltToggle(){
    if(operatorController.getRawButton(4)){
      if(!toggleOuterIntakeTiltPressed){
        toggleOuterIntakeTiltOn = !toggleOuterIntakeTiltOn;
        toggleOuterIntakeTiltPressed = true;
        }
        }else{
          toggleOuterIntakeTiltPressed = false;
        }
     }
}
