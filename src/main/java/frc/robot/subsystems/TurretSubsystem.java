package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import screech.control.Utilities;
import screech.models.PidConstants;
import screech.sensors.AS5600EncoderPwm;

/**
 * Does turret stuff.
 */
public class TurretSubsystem extends Subsystem implements PIDOutput, PIDSource {
  public WPI_TalonSRX Rotator;
  public AS5600EncoderPwm Encoder;
  public PIDController RotationPID;
  public boolean LockedOn = false;

  public TurretSubsystem() {
    // Robot.Debug("");
    // Robot.Debug("");
    // Robot.Debug("[TURRET] Initializing turret subsystem...");

    // Turret rotator motor
    try {
      Rotator = new WPI_TalonSRX(RobotMap.Turret_Rotator);
      Rotator.configOpenloopRamp(0.1);
      Rotator.configPeakCurrentDuration(100);
      Rotator.configPeakCurrentLimit(30);
      Rotator.configContinuousCurrentLimit(20);
      Rotator.enableCurrentLimit(true);
      Rotator.setInverted(true);

      Encoder = new AS5600EncoderPwm(Rotator.getSensorCollection());
      // Robot.Debug("[TURRET] Rotator motor and encoder ready");
    } catch (Exception ex) {
      DriverStation.reportError("[TURRET] Error initializing/configuring turret rotator: " + ex.getMessage(), true);
      // Robot.Debug("[TURRET] Error initializing/configuring turret rotator: " + ex.getMessage());
      throw ex;
    }

    // Rotator PID
    try {
      PidConstants rotatorPid = RobotMap.PID_Turret_Rotator;
      RotationPID = new PIDController(rotatorPid.kP, rotatorPid.kI, rotatorPid.kD, this, this);
      RotationPID.setSubsystem("Turret");
      RotationPID.setInputRange(0, 360);
      RotationPID.setOutputRange(-0.6, 0.6);
      RotationPID.setAbsoluteTolerance(0.5);
      RotationPID.setContinuous(true);
      SmartDashboard.putData(RotationPID);
      // Robot.Debug("[TURRET] Rotation PID controller enabled");
    } catch (Exception ex) {
      DriverStation.reportError("[TURRET] Error configuring turret rotation PID controller: " + ex.getMessage(), true);
      // Robot.Debug("[TURRET] Error configuring turret rotation PID controller: " + ex.getMessage());
      throw ex;
    }

    // Robot.Debug("[TURRET] Turret subsystem initialized");
    // Robot.Debug("");
    // Robot.Debug("");
  }

  public double GetHeading() {
    return Encoder.getPwmPosition();
  }

  public double GetAngle() {
    var pos = GetHeading();
    double adjPos = pos - RobotMap.TurretRotationLowPositionLimit;

    // Calculate to an angle where leftmost limit = 0°
    double angleAdjSlope = 360d / 4096d;
    double leftAngle = angleAdjSlope * adjPos;

    return leftAngle;
  }

  public void ToggleLockOn() {
    if (LockedOn) {
      LockedOn = false;
      RotationPID.setEnabled(false);
    } else {
      LockedOn = true;
      RotationPID.setEnabled(true);
    }
  }

  public void SetRotationWithLimelightOffset(double targetOffset, double manualAdjustment) {
    var targetAngle = GetAngle() + (targetOffset * 2);
    SetAngle(targetAngle + Utilities.Deadband(manualAdjustment, 0.1));
  }

  public void SetAngle(double angle) {
    if (angle > RobotMap.TurretRotationHighAngleLimit) {
      angle = RobotMap.TurretRotationHighAngleLimit;
    }

    if (angle < RobotMap.TurretRotationLowAngleLimit) {
      angle = RobotMap.TurretRotationLowAngleLimit;
    }

    if (!RotationPID.isEnabled()) {
      SetPIDEnabled(true);
    }

    RotationPID.setSetpoint(angle);
  }

  public void SetPIDEnabled(boolean enabled) {
    RotationPID.setEnabled(enabled);
  }

  public void SetPIDValues(PidConstants pid) {
    RotationPID.setPID(pid.kP, pid.kI, pid.kD);
  }

  public void RotateManually(double speed) {
    var cushion = 20 + (Math.abs(speed) * 25);
    var angle = GetAngle();

    var speedDirectionRight = speed > 0;
    var speedDirectionLeft = speed < 0;
    
    var proximityToLeftLimit = Math.abs(RobotMap.TurretRotationLowAngleLimit - angle);
    var proximityToRightLimit = Math.abs(RobotMap.TurretRotationHighAngleLimit - angle);

    if (speedDirectionLeft && proximityToLeftLimit < cushion) {
      System.out.println("[TURRET] LEFT LIMIT HIT");
      speed = 0;
    } else if (speedDirectionRight && (proximityToRightLimit < (cushion + 10) || proximityToRightLimit > 340)) {
      System.out.println("[TURRET] RIGHT LIMIT HIT");
      speed = 0;
    }
    
    if (proximityToLeftLimit < 45 || (proximityToLeftLimit < cushion || proximityToLeftLimit > 340)) {
      speed *= 0.5;
    }

    Rotator.set(ControlMode.PercentOutput, Utilities.Deadband(speed, 0.1));
  }

  public void StopAllMotors() {
    if (RotationPID.isEnabled()) {
      RotationPID.disable();
    }

    Rotator.set(ControlMode.Disabled, 0);
  }

  // PID methods

  @Override
  public void pidWrite(double output) {
    RotateManually(output);
  }

  @Override
  public void setPIDSourceType(PIDSourceType pidSource) {
    return;
  }

  @Override
  public PIDSourceType getPIDSourceType() {
    return PIDSourceType.kDisplacement;
  }

  @Override
  public double pidGet() {
    return GetAngle();
  }

  // Default command
  @Override
  public void initDefaultCommand() {
    // setDefaultCommand(new TurretPeriodicCommands());
  }
}
