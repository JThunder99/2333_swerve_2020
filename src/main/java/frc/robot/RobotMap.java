package frc.robot;

import screech.drive.swerve.SwerveMap;
import screech.models.PidConstants;

public final class RobotMap {
    // SwerveMap
    public static SwerveMap SwerveMap = new SwerveMap(
        10, 12, 15, 16,
        11, 13, 14, 17,
        0, 1, 2, 3,
        //FR    FL   RR    RL
        698, 1970, 1132, 3476,
        //Analog input from 0-5 * .8192
        21.5, 22.875,
        new PidConstants(0.01, 0.008, 0.018)
    );

    public static final double SpeedDeadband = 0.05;
    // public static double SpeedReduction;
    public static double SpeedGain;
    
    public static final int Turret_Rotator = 22;
    // public static final int Turret_Flywheel1 = 18;
    // public static final int Turret_Flywheel2 = 19;
    // public static final PidConstants PID_Turret_Flywheel = new PidConstants(0, 0, 0);
    public static final PidConstants PID_Turret_Rotator = new PidConstants(0.012, 0, 0);
    
    // Turret constants
    public static final int TurretRotationStartPosition = 2415;
    public static final int TurretRotationLowPositionLimit = 1445;
    public static final int TurretRotationLowAngleLimit = -41;
    public static final int TurretRotationHighPositionLimit = 3713;
    public static final int TurretRotationHighAngleLimit = 212;
    public static final int TurretRotationForwardPosition = 3040;
}